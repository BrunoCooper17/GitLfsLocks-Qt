#include "OperationMessageDialog.h"

#include<QLabel>
#include<QVBoxLayout>
#include<QHBoxLayout>

OperationMessageDialog::OperationMessageDialog(QWidget *parent) : QDialog(parent)
{
    QVBoxLayout* dialogLayout = new QVBoxLayout();

    QHBoxLayout* operationLayout = new QHBoxLayout();
    operationLabel = new QLabel();
    indexLabel = new QLabel();

    operationLayout->addStretch();
    operationLayout->addWidget(operationLabel);
    operationLayout->addWidget(indexLabel);
    operationLayout->addStretch();

    QHBoxLayout* fileLayout = new QHBoxLayout();
    fileLabel = new QLabel();

    fileLayout->addStretch();
    fileLayout->addWidget(fileLabel);
    fileLayout->addStretch();

    dialogLayout->addLayout(operationLayout);
    dialogLayout->addLayout(fileLayout);

    setLayout(dialogLayout);

    Qt::WindowFlags flags = Qt::Dialog | Qt::WindowTitleHint | Qt::WindowStaysOnTopHint;
    setWindowFlags(flags);
}

void OperationMessageDialog::UpdateIndex(int index) {
    indexLabel->setText(QString().setNum(index) + " of " + QString().setNum(maxIndex));
    update();
}

void OperationMessageDialog::UpdateMaxIndex(int index, int maxIndex) {
    this->maxIndex = maxIndex;
    UpdateIndex(index);
}

void OperationMessageDialog::UpdateOperation(QString operation) {
    operationLabel->setText(operation);
    update();
}

void OperationMessageDialog::UpdateFilename(QString file) {
    fileLabel->setText(file);
    update();
}
