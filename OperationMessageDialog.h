#ifndef OPERATIONMESSAGEDIALOG_H
#define OPERATIONMESSAGEDIALOG_H

#include<QDialog>

class OperationMessageDialog : public QDialog
{
    Q_OBJECT
public:
    OperationMessageDialog(QWidget *parent = nullptr);

    void UpdateIndex(int index);
    void UpdateMaxIndex(int index, int maxIndex);
    void UpdateOperation(QString operation);
    void UpdateFilename(QString file);

private:
    class QLabel* operationLabel;
    class QLabel* indexLabel;
    class QLabel* fileLabel;

    int maxIndex;
};

#endif // OPERATIONMESSAGEDIALOG_H
