#include "MainWindow.h"
#include <QApplication>

#include<QMetaType>
#include"MiscStructures.h"

int main(int argc, char *argv[])
{
    qRegisterMetaType<DataFilelock>();
    qRegisterMetaType<QMap<QString, QVector<DataFilelock>>>();

    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
