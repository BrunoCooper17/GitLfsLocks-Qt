#include "MapMiscOperations.h"
#include<QDebug>

void MapMiscOperations::PrintMapData(QMap<QString, QVector<DataFilelock> > &Map) {
    for (auto& mapIterator : Map)
        for (auto& vectorIterator : mapIterator)
            qDebug() << vectorIterator.Owner << " " << vectorIterator.bLocked << " " << vectorIterator.AssetName;
}

void MapMiscOperations::InsertDataInMap(QMap<QString, QVector<DataFilelock>> &Map, DataFilelock &Data) {
    if (!Map.contains(Data.Owner))
        Map[Data.Owner] = QVector<DataFilelock>();
    Map[Data.Owner].append(Data);
}

void MapMiscOperations::RemoveDataInMap(QMap<QString, QVector<DataFilelock>> &Map, QString IdData) {
    bool bEndSearch = false;
    for (auto& mapIterator : Map) {
        if (bEndSearch)
            break;

        for(int currentIndex = 0; currentIndex < mapIterator.size(); currentIndex++) {
            if (mapIterator.at(currentIndex).Id == IdData) {
                mapIterator.removeAt(currentIndex);
                bEndSearch = true;
                break;
            }
        }
    }
}

void MapMiscOperations::SortDataInMap(QMap<QString, QVector<DataFilelock> > &Map) {
    for (auto& mapIterator : Map) {
        for(int currentIndex = 0; currentIndex < mapIterator.size()-1; currentIndex++) {
            DataFilelock currentData = mapIterator.at(currentIndex);
            for(int swapIndex = currentIndex+1; swapIndex < mapIterator.size(); swapIndex++) {
                DataFilelock swapData = mapIterator.at(swapIndex);
                if (swapData.AssetName < currentData.AssetName) {
                    mapIterator.removeAt(swapIndex);
                    mapIterator.insert(currentIndex, swapData);
                    currentData = swapData;
                }
            }
        }
    }
}
