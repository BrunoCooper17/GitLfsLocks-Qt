#ifndef WATCHDOGLOCKS_H
#define WATCHDOGLOCKS_H

#include <QObject>

#include<QMap>
#include<QVector>

#include"MiscStructures.h"

class WatchdogLocks : public QObject
{
    Q_OBJECT
public:
    explicit WatchdogLocks(QObject *parent = nullptr);

    void SetCurrentWorkspace(QString NewWorkspace);

public slots:
    void WatchdogWork();

signals:
    void ChangesFounded(QMap<QString, QVector<DataFilelock>> CurrentData,
                        QMap<QString, QVector<DataFilelock>> ReleaseData,
                        QMap<QString, QVector<DataFilelock>> NewLockedData);

private:
    QString currentWorkspace;
    bool bFirstTry = true;

    QMap<QString, QVector<DataFilelock>> CurrentData;
    QMap<QString, QVector<DataFilelock>> ReleaseData;
    QMap<QString, QVector<DataFilelock>> NewLockedData;


    //---------------- FUNCTIONS ----------------
    void InitFilesLockedMaps();
    void RunLockChecking();
    //-------------------------------------------

};

#endif // WATCHDOGLOCKS_H
