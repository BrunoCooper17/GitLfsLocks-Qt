#include "MainWindow.h"
#include "ui_MainWindow.h"

#include<QCloseEvent>
#include<QMenu>
#include<QAction>
#include<QSystemTrayIcon>

#include<QFileDialog>
#include<QStandardPaths>
#include<QMessageBox>
#include<QLayoutItem>

#include<QProcess>

#include<QString>
#include<QDebug>

// Drag and drop
#include<QUrl>
#include<QMimeData>
#include<QDragEnterEvent>

#include"RowLockAssets.h"
#include"WatchdogLocks.h"
#include"LockMessageDialog.h"
#include"MapMiscOperations.h"
#include"OperationMessageDialog.h"
#include"GitWorker.h"

QSemaphore g_GitLfsOperation(1);
QSemaphore g_LockFilesOperation(1);

#if defined (Q_OS_LINUX)
#include<QApplication>
#endif

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    auto exitAction = new QAction(tr("&Exit"), this);
    connect(exitAction, &QAction::triggered, [this](){ bClosing = true; close(); });

    QMenu* trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(exitAction);

    sysTrayIcon = new QSystemTrayIcon(this);
    sysTrayIcon->setContextMenu(trayIconMenu);
    sysTrayIcon->setIcon(AppIcon);

    connect(sysTrayIcon, &QSystemTrayIcon::activated, [this](QSystemTrayIcon::ActivationReason reason) {
        if (reason == QSystemTrayIcon::Trigger) {
            if(isVisible()) {
                hide();
            }
            else {
                show();
            }
        }
    });

    connect(sysTrayIcon, &QSystemTrayIcon::messageClicked, [this]() {
        LockMessageDialog* tempDialog = new LockMessageDialog(this);
        tempDialog->SetDialogMessage(messageNotification);
        tempDialog->show();
    });

    sysTrayIcon->show();

    // Name & Icon
    setWindowTitle(AppName);
    setWindowIcon(AppIcon);
    setAcceptDrops(true);

    ui->setupUi(this);

    // Prepare the ScrollArea
    AssetsRowLayout = new QVBoxLayout(ui->scrollArea);
    AssetsRowLayout->addStretch();
    ui->scrollAreaWidgetContent->setLayout(AssetsRowLayout);

    // TODO: Save the last Workspace
    UnlockInterface(IsAValidDirection(currentWorkspaceDirectory));

    // Current Git User Name
    GitUserName = GetGitUserName();
//    qDebug() << GitUserName;

    // Thread that verifies the locks in the current repository
    WatchdogWorker = new WatchdogLocks();
    WatchdogWorker->moveToThread(&WatchdogThread);
    connect(this, &MainWindow::StartWork, WatchdogWorker, &WatchdogLocks::WatchdogWork);
    connect(WatchdogWorker, &WatchdogLocks::ChangesFounded, this, &MainWindow::OnChangesFounded);
    WatchdogThread.start();
    emit StartWork();

    // Git operations dialog creation
    gitDialogProgress = new OperationMessageDialog(this);
}

void MainWindow::closeEvent(QCloseEvent *event) {
    if (bClosing) {
        event->accept();
        WatchdogThread.requestInterruption();
#if defined (Q_OS_LINUX)
        QApplication::quit();
#endif
    }
    else {
        this->hide();
        event->ignore();
    }
}

MainWindow::~MainWindow()
{
    delete ui;

    WatchdogThread.quit();
    WatchdogThread.wait();
}



void MainWindow::OnChangesFounded(QMap<QString, QVector<DataFilelock>> CurrentData,
                                  QMap<QString, QVector<DataFilelock>> ReleaseData,
                                  QMap<QString, QVector<DataFilelock>> NewLockedData) {

    selectionSnapshot.clear();
    for (int index=0; index < AssetsRowLayout->count(); index++) {
        RowLockAssets* tempRowLockAssets = dynamic_cast<RowLockAssets*>(AssetsRowLayout->itemAt(index)->widget());
        if (tempRowLockAssets && tempRowLockAssets->GetAssetOwner() == GitUserName) {
            selectionSnapshot.insert(tempRowLockAssets->GetAssetName(), tempRowLockAssets->IsSelected());
        }
    }

    cacheDataBuffer = CurrentData;
    ClearLayout(AssetsRowLayout);

    UpdateDataShown(cacheDataBuffer, selectionSnapshot, searchCriteria, ui->SearchLineEdit->text());

    messageNotification = "";
    int FilesLocked = 0, FilesUnlocked = 0;

    if (ReleaseData.size())
        messageNotification += "Unlocked Asset:";
    for (auto& mapIterator : ReleaseData) {
        for (auto& vectorIterator : mapIterator) {
            messageNotification += "\n * " + vectorIterator.AssetName;
            FilesUnlocked++;
        }
    }

    for (auto mapIterator = NewLockedData.constBegin(); mapIterator != NewLockedData.constEnd(); mapIterator++) {
        if (messageNotification.size() != 0)
            messageNotification += "\n";
        if (mapIterator.key() == GitUserName)
            continue;
        messageNotification += mapIterator.key() + " locked:";
        for (auto& vectorIterator : mapIterator.value()) {
            messageNotification += "\n * " + vectorIterator.AssetName;
            FilesLocked++;
        }
    }

    if (messageNotification.size()) {
        QString sysTrayMessage;

        if (FilesLocked > 0 )
            sysTrayMessage += QString().setNum(FilesLocked) + " File(s) locked.";

        if (FilesUnlocked > 0 ) {
            if (sysTrayMessage.size() > 0)
                sysTrayMessage += "\n";

            sysTrayMessage += QString().setNum(FilesUnlocked) + " File(s) unlocked.";
        }

        sysTrayIcon->showMessage(AppName, sysTrayMessage, AppIcon);
    }
}

void MainWindow::OnFileOperationStarted(int index, QString file) {
    gitDialogProgress->UpdateIndex(index+1);
    gitDialogProgress->UpdateFilename(file);
}

void MainWindow::OnFileOperationFinished(int index, QString file, int result) {
    gitDialogProgress->UpdateIndex(index+1);
    gitDialogProgress->UpdateFilename(file);

    switch (gitLfsOperation) {
    case EGitLfsOperations::Lock:
        // Append to the list.
        if (result == 0) {
            DataFilelock tempData;
            tempData.Id = "ID:?????";
            tempData.Owner = GitUserName;
            tempData.AssetName = file;
            tempData.bLocked = true;

            MapMiscOperations::InsertDataInMap(cacheDataBuffer, tempData);
            MapMiscOperations::SortDataInMap(cacheDataBuffer);
            ClearLayout(AssetsRowLayout);
            UpdateDataShown(cacheDataBuffer, selectionSnapshot, searchCriteria, ui->SearchLineEdit->text());
        }
        else
            InvalidFiles.append(file);
        break;

    case EGitLfsOperations::Unlock:
        if (result == 0) {
            int index = 0;
            for (auto* data : rowsToUnlock) {
                if (data->GetAssetName() == file) {
                    RemoveItemInLayout(AssetsRowLayout, data);
                    MapMiscOperations::RemoveDataInMap(cacheDataBuffer, data->GetAssetId());
                    rowsToUnlock.removeAt(index);
                    AssetsRowLayout->update();
                    break;
                }
                index++;
            }
        }
        else
            InvalidFiles.append(file);
        break;
    }
}

void MainWindow::OnAllOperationFinished() {
    gitDialogProgress->hide();

    UnlockInterface(true);
    ui->ChangeWorkDirButton->setEnabled(true);

    InvalidFiles.sort();
    ReportLockWarningsToUser(InvalidFiles);

    g_GitLfsOperation.release();
}



void MainWindow::dragEnterEvent(QDragEnterEvent* event) {
    QMainWindow::dragEnterEvent(event);

//    qDebug() << "Enter Drop event? " << event->mimeData()->urls();

    if (IsAValidDirection(currentWorkspaceDirectory) && DragFileHasValidUrls(event->mimeData()->urls()))
        event->acceptProposedAction();
}

void MainWindow::dropEvent(QDropEvent* event) {
    QMainWindow::dropEvent(event);

//    qDebug() << "Drop event? " << event->mimeData()->urls();

    QStringList filesToLock, invalidFiles;
    filesToLock = ui->AssetsLockLineEdit->toPlainText().trimmed().split("\n");
    ui->AssetsLockLineEdit->setPlainText("");

    if (IsAValidDirection(currentWorkspaceDirectory))
        for (auto& tmpFile : event->mimeData()->urls())
            if (tmpFile.toLocalFile().startsWith(currentWorkspaceDirectory))
                filesToLock.append(tmpFile.toLocalFile());

    filesToLock = RemoveInvalidAssetsToLock(filesToLock, invalidFiles);
    filesToLock.sort();

    QString tempString;
    for (int index=0; index<filesToLock.size(); index++) {
        tempString += filesToLock.at(index);
        if (index+1 < filesToLock.size())
            tempString += "\n";
    }
    ui->AssetsLockLineEdit->setPlainText(tempString);
}



void MainWindow::on_ChangeWorkDirButton_clicked()
{
    QString workDirectory = QFileDialog::getExistingDirectory(this, tr("Choose Workspace Directory"), QStandardPaths::standardLocations(QStandardPaths::HomeLocation).last());

    if (QDir(workDirectory + "/.git").exists()) {
        QMessageBox::information(this, AppName, tr("The workspace selected is a git directory"));
        currentWorkspaceDirectory = workDirectory;
        assetsSubfolder = GetSubfolder(currentWorkspaceDirectory);
        ui->WorkDirLineEdit->setText(currentWorkspaceDirectory);
        UnlockInterface(true);

        WatchdogWorker->SetCurrentWorkspace(currentWorkspaceDirectory);
    }
    else {
        QMessageBox::warning(this, AppName, tr("The workspace selected is not a git directory"));
        currentWorkspaceDirectory = "";
        assetsSubfolder = "";
        ui->WorkDirLineEdit->setText(currentWorkspaceDirectory);
        UnlockInterface(false);

        ClearLayout(AssetsRowLayout);
    }
}

void MainWindow::on_AssetsSelectButton_clicked()
{
    QStringList invalidFiles;
    QStringList filesToLock = ui->AssetsLockLineEdit->toPlainText().split("\n");

    QStringList filesSelected = QFileDialog::getOpenFileNames(this, tr("Choose files to lock"), currentWorkspaceDirectory + assetsSubfolder);
    if (filesSelected.size() > 0) {
        filesToLock.append(filesSelected);
    }
    filesToLock = RemoveInvalidAssetsToLock(filesToLock, invalidFiles);
    filesToLock.sort();

    QString tempString;
    for (int index=0; index<filesToLock.size(); index++) {
        tempString += filesToLock.at(index);
        if (index+1 < filesToLock.size())
            tempString += "\n";
    }
    ui->AssetsLockLineEdit->setPlainText(tempString);
}

void MainWindow::on_AssetsLockButton_clicked()
{
    QStringList filesToLock = ui->AssetsLockLineEdit->toPlainText().trimmed().split("\n");
    InvalidFiles.clear();
    filesToLock = RemoveInvalidAssetsToLock(filesToLock, InvalidFiles);
    ui->AssetsLockLineEdit->setPlainText("");

    if (filesToLock.size() > 0) {
        CurateFilesToLock(filesToLock, currentWorkspaceDirectory);

        gitLfsOperation = EGitLfsOperations::Lock;
        gitWorker = GetNewGitWorker(gitLfsOperation, currentWorkspaceDirectory);
        gitWorker->SetListFiles(filesToLock);

        UnlockInterface(false);
        ui->ChangeWorkDirButton->setEnabled(false);
        g_GitLfsOperation.acquire();

        gitDialogProgress->UpdateOperation("Locking file");
        gitDialogProgress->UpdateMaxIndex(0, filesToLock.size());
        gitDialogProgress->show();
        gitWorker->start();
    }
}

void MainWindow::on_AssetsUnlockButton_clicked()
{
    QStringList filesToUnlock;
    rowsToUnlock.clear();
    for (int index = 0; index < AssetsRowLayout->count(); index++) {
        RowLockAssets* tempRowLockAssets = dynamic_cast<RowLockAssets*>(AssetsRowLayout->itemAt(index)->widget());
        if (tempRowLockAssets) {
            if (tempRowLockAssets->IsSelected()) {
                filesToUnlock.append(tempRowLockAssets->GetAssetName());
                rowsToUnlock.push_back(tempRowLockAssets);
            }
            else
                tempRowLockAssets->setSelected(true);
        }
    }

    if (filesToUnlock.size() > 0) {
        gitLfsOperation = EGitLfsOperations::Unlock;
        gitWorker = GetNewGitWorker(gitLfsOperation, currentWorkspaceDirectory);
        gitWorker->SetListFiles(filesToUnlock);

        UnlockInterface(false);
        ui->ChangeWorkDirButton->setEnabled(false);
        InvalidFiles.clear();
        g_GitLfsOperation.acquire();

        gitDialogProgress->UpdateOperation("Unlocking file");
        gitDialogProgress->UpdateMaxIndex(0, filesToUnlock.size());
        gitDialogProgress->show();
        gitWorker->start();
    }
}

void MainWindow::on_SearchLineEdit_textEdited(const QString &arg1)
{
    ClearLayout(AssetsRowLayout);
    UpdateDataShown(cacheDataBuffer, selectionSnapshot, searchCriteria, arg1);
}

void MainWindow::on_ByContenRadioButton_clicked()
{
    UpdateSearchCriteria();
    ClearLayout(AssetsRowLayout);
    UpdateDataShown(cacheDataBuffer, selectionSnapshot, searchCriteria, ui->SearchLineEdit->text());
}

void MainWindow::on_ByOwnerRadioButton_clicked()
{
    UpdateSearchCriteria();
    ClearLayout(AssetsRowLayout);
    UpdateDataShown(cacheDataBuffer, selectionSnapshot, searchCriteria, ui->SearchLineEdit->text());
}



QStringList MainWindow::RemoveInvalidAssetsToLock(QStringList filesToLock, QStringList& invalidFiles) {
    filesToLock.removeDuplicates();
    for (int index=0; index < filesToLock.size(); index++) {
        QDir tmpDir(filesToLock.at(index));
        if (!tmpDir.canonicalPath().startsWith(currentWorkspaceDirectory)) {
            invalidFiles.append(filesToLock.at(index));
            filesToLock.removeAt(index);
            index--;
        }
    }
    return filesToLock;
}

void MainWindow::CurateFilesToLock(QStringList &FilesToLock, QString workDirectory) {
    for (int index=0; index < FilesToLock.size(); index++) {
        QDir tmpDir(FilesToLock.at(index));
        FilesToLock[index] = tmpDir.canonicalPath().remove(workDirectory + "/");
    }
}

QStringList MainWindow::LockFilesProcess(const QStringList FilesToLock, const QString workDirectory) {
    QStringList InvalidFiles;
    QProcess lockProcess(this);
    QStringList lockArguments;

    lockProcess.setProgram("git");
    lockArguments << "lfs" << "lock" << "";
    lockProcess.setWorkingDirectory(workDirectory);

    g_GitLfsOperation.acquire();

    for (int index=0; index < FilesToLock.size(); index++) {
        QDir tmpDir(FilesToLock.at(index));
        lockArguments.last() = tmpDir.canonicalPath().remove(workDirectory + "/");
        lockProcess.setArguments(lockArguments);

        qDebug() << "Executing: " << lockProcess.program() << " " << lockProcess.arguments();

        lockProcess.start();
        lockProcess.waitForFinished();

        qDebug() << "Exit code: " << lockProcess.exitCode();

        if (lockProcess.exitCode() != 0) {
            qDebug() << "Error locking file: " << tmpDir.canonicalPath();
            InvalidFiles.push_back(tmpDir.path());
        }
    }

    g_GitLfsOperation.release();

    return InvalidFiles;
}

void MainWindow::ReportLockWarningsToUser(const QStringList invalidFiles) {
    if (invalidFiles.size() > 0) {
        QString tempInvalidFiles;
        for (int index=0; index<invalidFiles.size(); index++) {
            tempInvalidFiles += invalidFiles.at(index);
            if (index+1 < invalidFiles.size())
                tempInvalidFiles += "\n";
        }

        QMessageBox messageBox;
        messageBox.setWindowTitle(AppName);
        messageBox.setWindowIcon(AppIcon);
        messageBox.setText(AppName + " Error: Some files couldn't be locked.");
        messageBox.setInformativeText("Verify that they are not locked yet, are part of the current workspace or even exist.");
        messageBox.setDetailedText(tempInvalidFiles);
        messageBox.setIcon(QMessageBox::Icon::Warning);
        messageBox.setStandardButtons(QMessageBox::Ok);
        messageBox.exec();
    }
}



bool MainWindow::IsAValidDirection(QString Direction) const {
    return Direction.size() > 0 && QDir(Direction).exists();
}

QString MainWindow::GetSubfolder(QString WorkDirectory) {
    if(QDir(WorkDirectory).entryList(QStringList("*.uproject")).size() > 0)
        return "/Content";
    else if (QDir(WorkDirectory + "/Assets").exists())
        return "/Assets";
    return "";
}

QString MainWindow::GetGitUserName() {
    QProcess gitNameProcess(this);
    QStringList lockArguments;

    lockArguments << "config" << "--global" << "user.name";
    gitNameProcess.setProgram("git");
    gitNameProcess.setArguments(lockArguments);
    gitNameProcess.start();

    gitNameProcess.waitForFinished();

    if (gitNameProcess.exitCode() == 0) {
        QString dataOutput = gitNameProcess.readAllStandardOutput();
        return dataOutput.toUtf8().replace("\n", "");
    }
    return "";
}

void MainWindow::UnlockInterface(bool bLockInterface) {
    ui->AssetsLockLineEdit->setEnabled(bLockInterface);
    ui->AssetsSelectButton->setEnabled(bLockInterface);
    ui->AssetsLockButton->setEnabled(bLockInterface);

    ui->SearchLineEdit->setEnabled(bLockInterface);
    ui->ByOwnerRadioButton->setEnabled(bLockInterface);
    ui->ByContenRadioButton->setEnabled(bLockInterface);

    ui->scrollArea->setEnabled(bLockInterface);
    ui->AssetsUnlockButton->setEnabled(bLockInterface);
}



bool MainWindow::DataPassFilter(DataFilelock RowInfo, ETypeSearch typeSearch, QString criteria) {
    switch (typeSearch) {
    case ETypeSearch::Owner:
        return RowInfo.Owner.contains(criteria);
        break;
    case ETypeSearch::Content:
        return RowInfo.AssetName.contains(criteria);
        break;
    }

    return true;
}

RowLockAssets* MainWindow::RequestRowLockWidget() {
    for(auto* tempRow : cacheRowLockAssets)
        if (tempRow->parent() == nullptr)
            return tempRow;

    RowLockAssets* newRow = new RowLockAssets();
    cacheRowLockAssets.append(newRow);
    return newRow;
}

RowLockAssets* MainWindow::InsertRowInLayout(DataFilelock RowInfo, bool bIsOwned) {
    RowLockAssets* newRow = RequestRowLockWidget();
    newRow->SetAssetInfo(RowInfo);
    newRow->setCanBeChecked(bIsOwned);
    AssetsRowLayout->insertWidget(AssetsRowLayout->count() - 1, newRow);

    return newRow;
}

void MainWindow::ClearLayout(QLayout *layout) {
    for (int index=0; index < layout->count(); index++) {
        QLayoutItem *item = layout->itemAt(index);
        if (item && dynamic_cast<RowLockAssets*>(item->widget())) {
            item = layout->takeAt(index);
            RemoveItemInLayout(item);
            index--;
        }
    }
}

void MainWindow::RemoveItemInLayout(QLayoutItem *item) {
    if (item && item->widget())
       item->widget()->setParent(nullptr);
}

void MainWindow::RemoveItemInLayout(QLayout *layout, RowLockAssets *item) {
    if (layout && item) {
        for (int index=0; index < layout->count(); index++)
            if (layout->itemAt(index)->widget() == item) {
                layout->takeAt(index);
                break;
            }
        item->setParent(nullptr);
    }
}



void MainWindow::UpdateSearchCriteria() {
    if (ui->ByOwnerRadioButton->isChecked())
        searchCriteria = ETypeSearch::Owner;
    else
        searchCriteria = ETypeSearch::Content;
}

void MainWindow::UpdateDataShown(QMap<QString, QVector<DataFilelock>> listLockAssets, QMap<QString, bool> selectionSnapshot, ETypeSearch typeSearch, QString criteria) {
    // Always put my own assets first (if we have locked files of course)
    if (listLockAssets.contains(GitUserName)) {
        for (auto& vectorIterator : listLockAssets.value(GitUserName)) {
            if(DataPassFilter(vectorIterator, typeSearch, criteria)) {
                RowLockAssets* tempNewRow = InsertRowInLayout(vectorIterator, true);
                if (selectionSnapshot.find(vectorIterator.AssetName) != selectionSnapshot.end())
                    tempNewRow->setSelected(selectionSnapshot.value(vectorIterator.AssetName));
            }
        }
    }

    // Fill with everyone else assets
    for (auto mapIterator = listLockAssets.constBegin(); mapIterator != listLockAssets.constEnd(); mapIterator++) {
        // Except my own assets (they are already in the list)
        if (mapIterator.key() == GitUserName)
            continue;
        for (auto& vectorIterator : mapIterator.value())
            if(DataPassFilter(vectorIterator, typeSearch, criteria))
               InsertRowInLayout(vectorIterator, false);
    }
}

bool MainWindow::DragFileHasValidUrls(QList<QUrl> Files) {
    for (auto& tmpFile : Files)
        if (tmpFile.toLocalFile().startsWith(currentWorkspaceDirectory))
            return true;
    return false;
}



GitWorker* MainWindow::GetNewGitWorker(EGitLfsOperations operation, QString workspaceDirectory) {
    GitWorker* tempGitWorker = new GitWorker();
    tempGitWorker->SetWorkspaceDirectory(workspaceDirectory);

    switch(operation) {
    case EGitLfsOperations::Lock:
        tempGitWorker->SetGitLfsOperation("lock");
        break;

    case EGitLfsOperations::Unlock:
        tempGitWorker->SetGitLfsOperation("unlock");
        break;
    }

    connect(tempGitWorker, &GitWorker::FileOperationStarted, this, &MainWindow::OnFileOperationStarted);
    connect(tempGitWorker, &GitWorker::FileOperationFinished, this, &MainWindow::OnFileOperationFinished);
    connect(tempGitWorker, &GitWorker::AllOperationFinished, this, &MainWindow::OnAllOperationFinished);
    connect(tempGitWorker, &GitWorker::finished, tempGitWorker, &QObject::deleteLater);

    return tempGitWorker;
}
