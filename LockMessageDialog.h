#ifndef LOCKMESSAGEDIALOG_H
#define LOCKMESSAGEDIALOG_H

#include<QDialog>

class LockMessageDialog : public QDialog
{
    Q_OBJECT
public:
    LockMessageDialog(QWidget *parent = nullptr);
    void SetDialogMessage(QString newMessage);

private:
    class QLabel* message;
};

#endif // LOCKMESSAGEDIALOG_H
