#include "LockMessageDialog.h"

#include<QVBoxLayout>
#include<QHBoxLayout>
#include<QLabel>
#include<QPushButton>

LockMessageDialog::LockMessageDialog(QWidget *parent) : QDialog(parent)
{
    QVBoxLayout* mainLayout = new QVBoxLayout();

    QHBoxLayout* titleLayout = new QHBoxLayout();
    titleLayout->addWidget(new QLabel(tr("Changes in files:")));
    titleLayout->addStretch();

    message = new QLabel();

    QPushButton* tempButton = new QPushButton(tr("Close"));
    connect(tempButton, &QPushButton::clicked, [this]{ close(); });

    QHBoxLayout* buttonLayout = new QHBoxLayout();
    buttonLayout->addStretch();
    buttonLayout->addWidget(tempButton);

    mainLayout->addLayout(titleLayout);
    mainLayout->addWidget(message);
    mainLayout->addLayout(buttonLayout);

    setLayout(mainLayout);

    Qt::WindowFlags flags = Qt::Dialog | Qt::WindowTitleHint | Qt::WindowContextHelpButtonHint | Qt::WindowCloseButtonHint | Qt::WindowStaysOnTopHint;
    setWindowFlags(flags);
}

void LockMessageDialog::SetDialogMessage(QString newMessage) {
    message->setText(newMessage);
    update();
}
