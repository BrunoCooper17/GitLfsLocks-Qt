#include "RowLockAssets.h"

#include<QHBoxLayout>
#include<QCheckBox>
#include<QLabel>

RowLockAssets::RowLockAssets(QWidget *parent) : QWidget(parent)
{
    QHBoxLayout* tempLayout = new QHBoxLayout();

    checkbox = new QCheckBox(this);
    labelID = new QLabel(this);
    labelAsset = new QLabel(this);
    labelOwner = new QLabel(this);

    checkbox->setCheckState(Qt::CheckState::Unchecked);

    tempLayout->addWidget(checkbox);
    tempLayout->addWidget(labelID);
    tempLayout->addWidget(labelOwner);
    tempLayout->addWidget(labelAsset);
    tempLayout->addStretch();

    setLayout(tempLayout);
}

RowLockAssets::~RowLockAssets() {
    ClearWidget(layout());
}

void RowLockAssets::SetAssetInfo(DataFilelock DataRow) {
    dataRow = DataRow;
    UpdateWidgetRow();
}

void RowLockAssets::SetAssetInfo(QString Id, QString AssetName, QString Owner) {
    dataRow.Id = Id;
    dataRow.AssetName = AssetName;
    dataRow.Owner = Owner;
    UpdateWidgetRow();
}

DataFilelock RowLockAssets::GetAssetInfo() const {
    return dataRow;
}

QString RowLockAssets::GetAssetId() const {
    return dataRow.Id;
}

QString RowLockAssets::GetAssetName() const {
    return dataRow.AssetName;
}

QString RowLockAssets::GetAssetOwner() const {
    return dataRow.Owner;
}

void RowLockAssets::setCanBeChecked(bool bSelectable) {
    checkbox->setEnabled(bSelectable);
//    checkbox->setVisible(bSelectable);
    setSelected(bSelectable);
}

void RowLockAssets::setSelected(bool bSelectable) {
    checkbox->setCheckState(bSelectable ? Qt::CheckState::Checked : Qt::CheckState::Unchecked);
}

bool RowLockAssets::IsSelected() const {
    return checkbox->isEnabled() && checkbox->isChecked();
}

void RowLockAssets::UpdateWidgetRow() {
    labelID->setText(dataRow.Id + "\t");
    labelOwner->setText(dataRow.Owner + "\t");
    labelAsset->setText(dataRow.AssetName);
}

void RowLockAssets::ClearWidget(class QLayout *layout) {
    QLayoutItem *item;
    while((item = layout->takeAt(0))) {
        if (item->layout()) {
            ClearWidget(item->layout());
            delete item->layout();
        }
        if (item->widget())
           delete item->widget();
        delete item;
    }
}
