#ifndef GITWORKER_H
#define GITWORKER_H

#include <QObject>
#include<QThread>
#include<QVector>

class GitWorker : public QThread
{
    Q_OBJECT
public:
    GitWorker() {}
    GitWorker(QString lfsOperation, QStringList listFiles) { operation = lfsOperation; filesToLock = listFiles; }

    void SetGitLfsOperation(QString lfsOperation) { operation = lfsOperation; }
    void SetListFiles(QStringList listFiles) { filesToLock = listFiles; }
    void SetWorkspaceDirectory(QString workspace) { workspaceDirectory = workspace; }

protected:
    void run() override;

signals:
    void FileOperationStarted(int index, QString file);
    void FileOperationFinished(int index, QString file, int result);
    void AllOperationFinished();

private:
    QString operation;
    QString workspaceDirectory;
    QStringList filesToLock;
};

#endif // GITWORKER_H
