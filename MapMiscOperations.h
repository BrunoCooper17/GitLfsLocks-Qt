#ifndef MAPMISCOPERATIONS_H
#define MAPMISCOPERATIONS_H

#include<QMap>
#include<QString>

#include"MiscStructures.h"

class MapMiscOperations
{
public:
    static void PrintMapData(QMap<QString, QVector<DataFilelock>> &Map);
    static void InsertDataInMap(QMap<QString, QVector<DataFilelock>> &Map, DataFilelock &Data);
    static void RemoveDataInMap(QMap<QString, QVector<DataFilelock>> &Map, QString IdData);
    static void SortDataInMap(QMap<QString, QVector<DataFilelock>> &Map);
};

#endif // MAPMISCOPERATIONS_H
