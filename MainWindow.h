#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include<QThread>
#include<QMap>
#include<QVector>
#include"MiscStructures.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    void closeEvent(QCloseEvent *event) override;

private slots:
    // Watchdog Signal!
    void OnChangesFounded(QMap<QString, QVector<DataFilelock>> CurrentData,
                          QMap<QString, QVector<DataFilelock>> ReleaseData,
                          QMap<QString, QVector<DataFilelock>> NewLockedData);

    // GitOperations Signals!
    void OnFileOperationStarted(int index, QString file);
    void OnFileOperationFinished(int index, QString file, int result);
    void OnAllOperationFinished();

signals:
    void StartWork();

protected:
    // Is valid data?
    virtual void dragEnterEvent(QDragEnterEvent* event) override;
    // Process drop data
    virtual void dropEvent(QDropEvent* event) override;

private slots:
    // Workspace selection
    void on_ChangeWorkDirButton_clicked();
    // Open file dialog to select files to lock
    void on_AssetsSelectButton_clicked();
    // Action Git lfs lock
    void on_AssetsLockButton_clicked();
    // Action Git lfs unlock
    void on_AssetsUnlockButton_clicked();
    // Search!
    void on_SearchLineEdit_textEdited(const QString &arg1);
    // Change search criteria
    void on_ByContenRadioButton_clicked();
    // Change search criteria
    void on_ByOwnerRadioButton_clicked();

private:
    Ui::MainWindow *ui;

    // Used to control the closing of the app (Minimize to tray)
    bool bClosing = false;

    // System tray icon reference
    class QSystemTrayIcon* sysTrayIcon;
    // The layout that will store all the assets locked.
    class QVBoxLayout* AssetsRowLayout;
    // Thread responsible to execute the command Git Lfs Locks
    QThread WatchdogThread;
    // Thread worker
    class WatchdogLocks* WatchdogWorker;
    // Rows to display (cache)
    QVector<class RowLockAssets*> cacheRowLockAssets;
    // Snapshot of Locked files.
    QMap<QString, QVector<DataFilelock>> cacheDataBuffer;
    // Snapshot of files marked to lock/unlock
    QMap<QString, bool> selectionSnapshot;

    // Name of the app
    const QString AppName = "GitLfsWatchdog";
    // Icon of the app
    const QIcon AppIcon = QIcon("://Icons/IconGitLfs.png");
    // Git user.name
    QString GitUserName = "";

    // Current workspace
    QString currentWorkspaceDirectory;
    // Used for projects made with UnrealEngine or Unity
    QString assetsSubfolder;
    // Last Notification Message
    QString messageNotification;
    // Current search criteria
    ETypeSearch searchCriteria;

    // Git Operations Thread worker.
    class GitWorker* gitWorker;
    // Current git operation been executed.
    EGitLfsOperations gitLfsOperation;
    // In case of unlock operation... store the rows to remove
    QVector<class RowLockAssets*> rowsToUnlock;
    // Dialog to show git lock/unlock progress
    class OperationMessageDialog* gitDialogProgress;
    // Invalid files (not existent, outside the workspace, already locked...)
    QStringList InvalidFiles;



    //---------------- FUNCTIONS ----------------
    QStringList RemoveInvalidAssetsToLock(QStringList FilesToLock, QStringList& InvalidFiles);
    void CurateFilesToLock(QStringList &FilesToLock, QString workDirectory);
    QStringList LockFilesProcess(const QStringList FilesToLock, const QString workDirectory);
    void ReportLockWarningsToUser(const QStringList InvalidFiles);

    bool IsAValidDirection(QString Direction) const;
    QString GetSubfolder(QString WorkDirectory);
    QString GetGitUserName();
    void UnlockInterface(bool bLockInterface);

    bool DataPassFilter(DataFilelock RowInfo, ETypeSearch typeSearch = ETypeSearch::Content, QString criteria = "");
    class RowLockAssets* RequestRowLockWidget();
    class RowLockAssets* InsertRowInLayout(DataFilelock RowInfo, bool bIsOwned);
    void ClearLayout(class QLayout *layout);
    void RemoveItemInLayout(class QLayoutItem* item);
    void RemoveItemInLayout(class QLayout* layout, class RowLockAssets* item);

    void UpdateSearchCriteria();
    void UpdateDataShown(QMap<QString, QVector<DataFilelock>> listRowLockAssets, QMap<QString, bool> selectionSnapshot, ETypeSearch typeSearch, QString criteria);
    bool DragFileHasValidUrls(QList<QUrl> Files);

    class GitWorker* GetNewGitWorker(EGitLfsOperations operation, QString workspaceDirectory);
    //-------------------------------------------
};
#endif // MAINWINDOW_H
