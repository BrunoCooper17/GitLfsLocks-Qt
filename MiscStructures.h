#ifndef MISCSTRUCTURES_H
#define MISCSTRUCTURES_H

#include<QString>
#include<QSemaphore>
#include<QMetaType>

extern QSemaphore g_GitLfsOperation;

enum class ETypeSearch {
    Content,
    Owner
};

enum class EGitLfsOperations {
    Lock,
    Unlock
};

typedef struct struct_data_filelock {
    QString Id;
    QString AssetName;
    QString Owner;
    bool bLocked;
} DataFilelock;

Q_DECLARE_METATYPE(DataFilelock);

#endif // MISCSTRUCTURES_H
