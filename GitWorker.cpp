#include "GitWorker.h"

#include<QProcess>
#include<QDebug>

void GitWorker::run() {
    for (int index = 0; index < filesToLock.size(); index++) {
        QProcess lockProcess;
        QStringList lockArguments;

        lockProcess.setWorkingDirectory(workspaceDirectory);
        lockProcess.setProgram("git");
        lockArguments << "lfs" << operation << filesToLock.at(index);
        lockProcess.setArguments(lockArguments);

        qDebug() << "git " << lockArguments;

        emit FileOperationStarted(index, filesToLock.at(index));

        lockProcess.start();
        lockProcess.waitForFinished();

        qDebug() << "Exit: " << lockProcess.exitCode() << " " << filesToLock.at(index);

        emit FileOperationFinished(index, filesToLock.at(index), lockProcess.exitCode());
    }

    QThread::msleep(250);
    emit AllOperationFinished();
}
