#ifndef ROWLOCKASSETS_H
#define ROWLOCKASSETS_H

#include"MiscStructures.h"
#include <QWidget>

class RowLockAssets : public QWidget
{
    Q_OBJECT
public:
    explicit RowLockAssets(QWidget *parent = nullptr);
    ~RowLockAssets();

    void SetAssetInfo(DataFilelock DataRow);
    void SetAssetInfo(QString Id, QString AssetName, QString Owner);
    DataFilelock GetAssetInfo() const;
    QString GetAssetId() const;
    QString GetAssetName() const;
    QString GetAssetOwner() const;

    void setCanBeChecked(bool bSelectable);
    void setSelected(bool bSelectable);
    bool IsSelected() const;

//signals:

private:
    class QCheckBox* checkbox;
    class QLabel* labelOwner;
    class QLabel* labelAsset;
    class QLabel* labelID;

    DataFilelock dataRow;

    //---------------- FUNCTIONS ----------------
    void UpdateWidgetRow();
    void ClearWidget(class QLayout *layout);
    //-------------------------------------------
};

#endif // ROWLOCKASSETS_H
