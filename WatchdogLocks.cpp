#include "WatchdogLocks.h"

#include<QThread>
#include<QProcess>
#include<QDir>

#include<QMap>
#include<QVector>

#include"MapMiscOperations.h"

#include<QDebug>

WatchdogLocks::WatchdogLocks(QObject *parent) : QObject(parent)
{

}

void WatchdogLocks::SetCurrentWorkspace(QString NewWorkspace) {
    if (currentWorkspace != NewWorkspace) {
// Clean the files located.
    }
    currentWorkspace = NewWorkspace;
}

void WatchdogLocks::WatchdogWork() {
    while(true) {
//        qDebug() << "Watchdog Work!";
        if (QThread::currentThread()->isInterruptionRequested()) {
            qDebug() << "Watchdog INTERRUPTED!";
            return;
        }
        if (currentWorkspace.size() && QDir(currentWorkspace).exists()) {
            InitFilesLockedMaps();
            g_GitLfsOperation.acquire();
            qDebug() << "Checking locks Start";
            RunLockChecking();
            qDebug() << "Checking locks End";
            g_GitLfsOperation.release();
            emit ChangesFounded(CurrentData, ReleaseData, NewLockedData);
        }
        QThread::currentThread()->sleep(10);
    }
}



void WatchdogLocks::InitFilesLockedMaps() {
    // In the current data fetched as locked, marked as unlocked by default (this is to check which ones were freed)
    for (auto& mapIterator : CurrentData) {
        for (auto& vectorIterator : mapIterator) {
            vectorIterator.bLocked = false;
        }
    }

    // Clean the buffer maps to notify if there where changes
    ReleaseData.clear();
    NewLockedData.clear();
}

void WatchdogLocks::RunLockChecking() {
    QProcess CheckAssetLockProcess;

    QString programName("git");
    QStringList arguments;

    arguments << "lfs" << "locks";
    CheckAssetLockProcess.setProgram(programName);
    CheckAssetLockProcess.setArguments(arguments);

    CheckAssetLockProcess.setWorkingDirectory(currentWorkspace);
    CheckAssetLockProcess.start();
    CheckAssetLockProcess.waitForFinished();

    if (CheckAssetLockProcess.exitCode() == 0) {
        DataFilelock tempData;
        QStringList Rows = QString(QString(CheckAssetLockProcess.readAllStandardOutput()).toUtf8()).split('\n');

        for (int index=0; index<Rows.size(); index++) {
            QStringList RowSplit = Rows.at(index).split('\t');
            if(RowSplit.size() > 1) {
//                qDebug() << RowSplit.at(2).trimmed() << " " << RowSplit.at(1).trimmed() << " " << RowSplit.at(0).trimmed();

                tempData.Id = RowSplit.at(2).trimmed();
                tempData.Owner = RowSplit.at(1).trimmed();
                tempData.AssetName = RowSplit.at(0).trimmed();
                tempData.bLocked = true;

                // Look in the current data if is already locked (by the same user/author).
                bool bAlreadyLocked = false;

//                qDebug() << "Looking for the asset";
                if (CurrentData.contains(tempData.Owner)) {
                    for(auto& vectorIterator : CurrentData[tempData.Owner]) {
                        if (tempData.AssetName == vectorIterator.AssetName) {
//                            qDebug() << "Already locked";
                            vectorIterator.bLocked = true;
                            bAlreadyLocked = true;
                            break;
                        }
                    }
                }

                if (!bAlreadyLocked) {
                    // New locked file.
//                    qDebug() << "New locked file!";
                    MapMiscOperations::InsertDataInMap(CurrentData, tempData);
                    MapMiscOperations::InsertDataInMap(NewLockedData, tempData);
                }
            }
        }

        // Looks for the unlocked files (bLocked == false)
        for (auto& mapIterator : CurrentData) {
            for (int index=0; index < mapIterator.size(); index++) {
                DataFilelock tempData = mapIterator.at(index);
                if (!tempData.bLocked) {
                    MapMiscOperations::InsertDataInMap(ReleaseData, tempData);
                    mapIterator.removeAt(index);
                    index--;
                }
            }
        }

        MapMiscOperations::SortDataInMap(CurrentData);
        MapMiscOperations::SortDataInMap(ReleaseData);
    }
}
