# GitLfsLocks Qt

My attempt to do an app that helps us to use git lfs lock feature.

Main focus right now is make easier to use the lock feature of git lfs (lock/locks/unlock commands) for none command line users.

## Features:
- Batch locking/unlocking files.
- Monitoring locked files in a repository.
- Validates that the files that are going to be locked, are located in the repository directory.
- Drag&Drop files to be locked.

Right now, the app asumes that the repository is already configurated to work with lfs and lfs lock.

The app is build using Qt 5.14.2.

---

Programmed by:
 - Jesús Mastache Caballero (BrunoCooper17)

[Portafolio](https://brunocooper17.gitlab.io/portafolio/projects)
[Twitter](https://twitter.com/BrunoCooper_17)
[LinkedIn](https://www.linkedin.com/in/jesus-mastache-25265952/)